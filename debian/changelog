jgraph (83-25) unstable; urgency=medium

  * Team upload.

  [ Andreas Tille ]
  * Take over into Debian Science maintenance (Closes: #759731).

  [ Petter Reinholdtsen ]
  * Trim trailing whitespace.
  * Bump debhelper from deprecated 9 to 13.
  * Set debhelper-compat version in Build-Depends.
  * Drop unnecessary dh arguments: --parallel.

 -- Petter Reinholdtsen <pere@debian.org>  Fri, 15 Nov 2024 14:58:34 +0100

jgraph (83-24) unstable; urgency=medium

  * QA upload.
  * Convert to source format 3.0. (Closes: #1007588)

 -- Bastian Germann <bage@debian.org>  Thu, 24 Aug 2023 10:45:04 +0000

jgraph (83-23) unstable; urgency=medium

  * QA upload.
  * Set maintainer to the QA group.
  * Replace debian/rules with dh (closes: #800253)
  * Update FSF's address.

 -- Adam Borowski <kilobyte@angband.pl>  Mon, 15 Feb 2016 03:59:47 +0100

jgraph (83-22) unstable; urgency=low

  * Fixed manpage typos. Thanks to Francesco Poli. (closes: #352617)

 -- Pedro Zorzenon Neto <pzn@debian.org>  Sun, 12 Feb 2006 22:36:59 -0200

jgraph (83-21) unstable; urgency=low

  * Fixed package description. Thanks to Martin Michlmayr.
    (closes: #218285)
  * Used "protoize" command to change sources from old-C style to ANSI
  * Upgraded Debian Standards Version.
  * Fixed many "passing arg X of `function_name' from incompatible
    pointer type" warnings.

 -- Pedro Zorzenon Neto <pzn@debian.org>  Sat, 30 Jul 2005 16:29:45 -0300

jgraph (83-20) unstable; urgency=low

  * bugfix - hash label fails with logarithmic axis
    sometimes. added manpage information telling the user
    what to do in this situation. (Closes: #140063)
  * bugfix - wishlist, changed some information in manpage.
    Thanks to Neil Spring <nspring@cs.washington.edu>.
    (Closes: #140064)

 -- Pedro Zorzenon Neto <pzn@debian.org>  Sat,  1 Jun 2002 12:08:02 -0300

jgraph (83-19) unstable; urgency=low

  * bugfix - GSview32 (version 4.0) couldn't understand EPS
    header. Now jgraph produces EPS by default, and command
    line option "-P" will produce PS files. (Closes: #126921)

 -- Pedro Zorzenon Neto <pzn@debian.org>  Mon,  7 Jan 2002 19:06:36 -0300

jgraph (83-18) unstable; urgency=low

  * changed mail address of author Jim Plank in README.

 -- Pedro Zorzenon Neto <pzn@debian.org>  Mon, 26 Nov 2001 14:10:04 -0300

jgraph (83-17) unstable; urgency=low

  * bugfix - segfaults on multiline labels - thanks to
    Carlos <angus@quovadis.com.ar> that reported and sent the patch.
    (Closes: #120507)

 -- Pedro Zorzenon Neto <pzn@debian.org>  Thu, 22 Nov 2001 12:41:48 -0300

jgraph (83-16) unstable; urgency=low

  * added support for Iso Latin1 character set and manpage
    section explaining how to use it.
    (Closes: #111992)
  * added expand bounding box support. see manpage (JGRAPH_BORDER)
  * changed 02139 FSF postal code in copyright to 0_2_1_3_9. Lintian
    will not complain about it anymore.

 -- Pedro Zorzenon Neto <pzn@debian.org>  Wed, 12 Sep 2001 16:30:54 -0300

jgraph (83-15) unstable; urgency=low

  * added jgraph/LaTeX integration section in manpage.
    Thanks to Ajay Shah <ajayshah@igidr.ac.in>.
    (Closes: #108260)
  * changed upstream author email in manpage.

 -- Pedro Zorzenon Neto <pzn@debian.org>  Wed, 15 Aug 2001 19:24:36 -0300

jgraph (83-14) unstable; urgency=low

  * changed the mantainer e-mail in debian/control file,
    that I forgot in previous upload.

 -- Pedro Zorzenon Neto <pzn@debian.org>  Wed,  8 Aug 2001 18:10:37 -0300

jgraph (83-13) unstable; urgency=low

  * mantainer e-mail changed.

 -- Pedro Zorzenon Neto <pzn@debian.org>  Mon,  6 Aug 2001 13:35:51 +0000

jgraph (83-12) unstable; urgency=low

  * New maintainer, see message:
    http://lists.debian.org/debian-devel-0107/msg00289.html
  * debian dir rewritten to use debhelper version 3
  * File debian/jgraph.compress removed. Compression of example
    files changed to dh_compress default behavior. (compressed
    only if greater than 4KBytes)

 -- Pedro Zorzenon Neto <pzn@terra.com.br>  Mon, 16 Jul 2001 08:25:32 -0300

jgraph (83-11) unstable; urgency=low

  * New maintainer (Closes: #68113, #82867)
  * Debian Policy 3.5.0.0 conformance
  * debian/rules rewrite with debhelper

 -- Chuan-kai Lin <cklin@oink.cc.ntu.edu.tw>  Wed, 31 Jan 2001 12:39:21 +0800

jgraph (83-10) unstable; urgency=low

  * cleaned up debian/rules and usr/doc/jgraph
  * removed debmake build dependency
  * recompiled for libc6

 -- Rob Browning <rlb@cs.utexas.edu>  Thu, 21 Aug 1997 00:02:42 -0500

jgraph (83-9) unstable; urgency=low

  * New maintainer (previous was  Bill Mitchell <mitchell@debian.org>)
  * Converted to new packaging standards
  * Added complex-examples

 -- Rob Browning <osiris@cs.utexas.edu>  Tue, 28 Jan 1997 00:11:29 -0600

jgraph (83-8) unstable; urgency=low

  * source package multi-architecture compatability changes

 -- Rob Browning <osiris@cs.utexas.edu>  Mon, 27 Jan 1997 22:37:41 -0600

jgraph (83-7) unstable; urgency=low

  * rebuilt for elf

 -- Rob Browning <osiris@cs.utexas.edu>  Mon, 27 Jan 1997 22:37:31 -0600

jgraph (83-6) unstable; urgency=low

  * (skipped)

 -- Rob Browning <osiris@cs.utexas.edu>  Mon, 27 Jan 1997 22:37:21 -0600

jgraph (83-5) unstable; urgency=low

  * Reposition jgraph.1 from /usr/man to /usr/man/man1.
  * use debian.makefile instead of makefile from upstream package

 -- Rob Browning <osiris@cs.utexas.edu>  Mon, 27 Jan 1997 22:37:06 -0600

jgraph (83-4) unstable; urgency=low

  * Redid debian.rules in Ian Murdock format.
  * Cleaned up description and extended descripting in control file.

 -- Rob Browning <osiris@cs.utexas.edu>  Mon, 27 Jan 1997 22:35:43 -0600

jgraph (83-3) unstable; urgency=low

  * More bolding fixes in jgraph.1.
  * Changed CFLAGS in makefile from debug to production flags

 -- Rob Browning <osiris@cs.utexas.edu>  Mon, 27 Jan 1997 22:35:12 -0600

jgraph (83-2) unstable; urgency=low

  * Changed line 1171 of jgraph.1 from "\fBoff\R" to "\fBoff\fR"
  * Changed /usr/doc/examples/jgraph/Makefile to clean up sin.o
    as well as the *.jps files on "make clean".

 -- Rob Browning <osiris@cs.utexas.edu>  Mon, 27 Jan 1997 22:34:42 -0600

jgraph (83-1) unstable; urgency=low

  * Initial Release.
  * added debian.* files
  * fixed many small problems with bolding in jgraph.1
  * added -O2 and -s compiler flags to CC macro in makefile
  * changed occurrances of "nawk" to "awk" in ex2.jgr and makefile
  * in jgraph.1, replaced "JGRAPH_DIR" with "/usr/doc/examples/jgraph"

 -- Rob Browning <osiris@cs.utexas.edu>  Mon, 27 Jan 1997 22:33:49 -0600
